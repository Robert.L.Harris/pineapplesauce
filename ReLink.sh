#!/bin/bash

# Re-enable previous mods
for i in `ls -1 /sd/modules | egrep -v 'Nano' `
do
  if [ -L /pineapple/modules/$i ];
  then
    echo Skipping /pineapple/modules/$i
  else
    echo Linking /sd/modules/$i /pineapple/modules/$i
    /bin/ln -s /sd/modules/$i /pineapple/modules/$i
  fi
done
