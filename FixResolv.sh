#!/bin/bash


case "$1" in
start)
    echo "search rdlg.net" > /etc/resolv.conf
    echo "domain rdlg.net" >> /etc/resolv.conf
    echo "nameserver 172.20.0.1" >> /etc/resolv.conf
    ;;
stop)
    echo "search lan" > /etc/resolv.conf
    echo "nameserver 127.0.0.1" >> /etc/resolv.conf
    ;;
restart)
    /etc/init.d/FixResolv.sh stop
    /etc/init.d/FixResolv.sh restart
    ;;
*)
    echo "Try start, stop or restart"
    exit
    exit
    ;;
esac
exit 0

