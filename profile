set -o vi

# Set the Terminal size
echo -en "\e[8;60;150t";


#
# Proxy Stuffs
#
#alias setproxy='export https_proxy="https://proxy.rdlg.net:3128"; export http_proxy="http://proxy.rdlg.net:3128"; export gopher_proxy="http://proxy.rdlg.net:3128"; export ftp_proxy="http://proxy.rdlg.net:3128"'
alias noproxy="unset https_proxy; unset http_proxy; unset gopher_proxy; unset ftp_proxy" 
#alias opkg="fixresolvconf; setproxy; /usr/bin/pineapple/opkg update; /usr/bin/pineapple/opkg"
#alias opkg="/usr/bin/pineapple/opkg update; /usr/bin/pineapple/opkg"
alias opkg="/usr/bin/pineapple/opkg"
alias fixresolvconf="/etc/init.d/resolvconf start"

export LANG=POSIX
WhoAmI="root"
Hostname="blackbird"
export TERM=xterm-256color
export PYTHONPATH="/sd/usr/lib/python2.7/site-packages/"

# Format History
export HISTSIZE=10000
export HISTTIMEFORMAT='[%F %T]??'

export PS1="\n$WhoAmI@$Hostname\n{\$?}:\$PWD>"


# Fix color and TERM for Less
export LESS='-iR -j4 --shift 5 -P ?n?f%f .?m(file %i of %m) ..?ltlines %lt-%lb?L/%L. :byte %bB?s/%s. .?e(END) ?x- Next\: %x.:?pB%pB\%..%t'


export EDITOR='/usr/bin/vim'
#export TZ=`cat /etc/timezone`
export LD_LIBRARY_PATH="/lib:/sd/lib:/usr/lib:/sd/usr/lib:/usr/local/lib:/sd/usr/local/lib:$LD_LIBRARY_PATH"

export PATH="$PATH:/sbin:/sd/sbin:/bin:/sd/bin:/usr/sbin:/sd/usr/sbin:/usr/local/bin:/sd/usr/local/bin:/root/bin:/usr/bin/pineapple/:/sd/usr/bin/pineapple:/usr/lib/git-core:/sd/usr/lib/git-core/"
export GIT_EXEC_PATH="/sd/usr/lib/git-core/"


#
# General Aliases
#
#
# Git Commands
#
alias gitcheckin="git add . && git status && echo \"\" && echo \"\" && echo \"Now do a \" && echo \"   git commit -m \'reason \'\" "
alias gitpull="git pull"
alias gitmpush="git push -u origin master"
alias gitpush="git push"
alias gitforcepull="git reset --hard origin/master"
alias gitcheckinevolution="gitcheckin; git commit -m 'Evolution'; gitpush"
# In case .git gets too big or out of hand
alias gitclean="git fsck; git prune"
alias gitdeepclean="git reflog expire --expire=now --all; git repack -ad; git prune"
#
#alias attach='~/bin/fixssh.sh; . ~/bin/fixssh; tmux attach -d'
SCREENRC=".screenrc."$Hostname;
alias screen='/usr/bin/screen -a -l -A -c ~/$SCREENRC'
alias attach='/usr/bin/screen -r -d -c $SCREENRC'
#
alias driftnet="sudo /usr/bin/driftnet -i eth4 -d /home/nomad/driftnet/"
alias oo="libreoffice"
#
alias profile='. /root/.profile'
alias sbb='sudo /bin/bash --rcfile /root/.profile'
alias vprofile='vi /root/.profile'
alias cpprofile='rsync -av /root/.profile /sd/root_home/.profile'
#
alias cdcapture='cd  /sd/modules/SiteSurvey/capture'
#

alias wifite2="cd /sd/wifite2 && ./Wifite.py -i wlan2"
alias wifite3="cd /sd/wifite2 && ./Wifite.py -i wlan3"
alias wifite="cd /sd/wifite2 && ./Wifite.py -i `ifconfig | egrep -v '^\s|^\t|^ |^$|br-lan|eth|lo|wlan0|wlan1' | awk '{print \$1}'`"
