#!/bin/bash

#
# Fix Running Environment
#
/root/sd/GetPkgs/FixEnvironment.sh

#
# Fix some packages and locations
#
/root/sd/GetPkgs/FixPackages.sh

#
# Fix installed modules and pull down some others
#
/root/sd/GetPkgs/FixModules.sh

