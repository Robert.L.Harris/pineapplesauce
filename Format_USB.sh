#!/bin/bash
#2018 - Zylla - (adde88@gmail.com)
 
[[ -f /tmp/usb_format.progress ]] && {
  exit 0
}
 
touch /tmp/usb_format.progress
 
umount /sd
swapoff /dev/sdb2
 
sleep 2
cat /pineapple/modules/Advanced/formatSD/fdisk_options | fdisk /dev/sdc
sleep 2
 
umount /sd
mkfs.ext4 /dev/sdc1
sleep 2
 
mkfs.ext4 /dev/sdc2
mkswap /dev/sdc2
 
mount /dev/sdc1 /sd
swapon /dev/sdc2
mount /dev/sdb1 /sd2
 
rm /tmp/usb_format.progress
