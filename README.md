# PineAppleSauce

Tools and add-ons for my PineApple.  You want to check all these out into /sd.

GetPkgs.sh - Main script to set everything up
ReLink.sh - used by GetPkgs.sh to fix links to modules on the SD card

Format_USB.sh
SetNetwork.sh

FixResolv.sh - Deprecated, don't use
