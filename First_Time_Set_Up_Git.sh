#!/bin/bash

echo
echo
echo
echo "Your memory card should be mounted at /sd"
echo
echo
echo

echo "Sleeping 15 Seconds for you to abort or go."
sleep 5

cd /tmp
mkdir /tmp/foobar
git clone git@gitlab.com:Robert.L.Harris/pineapplesauce.git foobar
rsync -av /foobar /sd
