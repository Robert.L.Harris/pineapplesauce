#!/bin/bash

#
# Where is our SD card mounted?  This is a standard but there may be one-offs
#
SD="sd"
SDMnt="/sd"
cd $SDMnt

#
# Lets actually load $PATH, etc
#
if [[ -f $SDMnt/root_home/.profile ]]; then
  echo "Sourcing $SDMnt/root_home/.profile"
  . $SDMnt/root_home/.profile
else
  echo 
  echo "For Pathing, you really need top do the following:"
  echo "mkdir $SDMnt/root_home"
  echo "cp /root/.profile $SDMnt/root_home/.profile"
  echo "If you don't have a .profile, there is an example profile_sample in this repo"
  echo 
  exit 255
fi

#
# Is our sd card mounted
#
/bin/mount | /bin/grep $SDMnt >/dev/null 2>&1
MountCheck=$?
if [ "$MountCheck" -ne "0" ]; then
  echo; echo
  echo SD card is not mounted.
  echo; echo
  exit 1
fi

#
# Fix Paths
#
export LD_LIBRARY_PATH="/lib:/sd/lib:/usr/lib:/sd/usr/lib//usr/local/lib:$LD_LIBRARY_PATH"
export PATH="$PATH:/sbin:/usr/sbin:/sd/usr/sbin:/usr/local/bin:/root/bin:/bin:/sd/bin/:/sd/usr/bin/:/usr/bin/pineapple/opkg"

#
# Is our root shell still horked?  Lets fix it
#
/bin/grep bin/ash /etc/passwd >/dev/null 2>&1
PWCheck=$?
if [ "$PWCheck" -ne "1" ]; then
  echo "Fixing Password entry for root"
  /bin/sed -e 's,bin/ash,bin/bash,' < /etc/passwd > /tmp/passwd
  /bin/mv /tmp/passwd /etc/passwd
fi

#
# Fix /root, due to space, lets make it a link to /sd/root_home
#
if [[ -d $SDMnt/root_home ]]; then
  echo "You have a $SDMnt/root_home, linking to /root"
  rm -rf /root
  ln -s /sd/root_home  /root
else
  echo 
  echo 
  echo "You do not have a $SDMnt/root_home, NOT linking to /root"
  echo "** I recommend rsyncing /root to /sd/root_home so you can keep persistent root files in /sd/root"
  echo 
  echo 
fi

#
# Lets fix our link to /root/sd
#
if [[ ! -h /root/sd ]]; then
  echo re-linking $SDMnt to /root/sd
  ln -s $SDMnt /root/sd
fi

#
# Is there a network Interface installed so we can install software and dependencies?
#
echo "Is there a network Interface installed?"
echo; echo
Interface=`iwconfig 2>&1 | egrep '802.11|usb' | egrep -v 'wlan0|wlan1' | awk {'print $1'}`
if [[ "$Interface" == "" ]]; then
  echo; echo;
  echo "  No available network interface found."
  echo "  Go into the Pineapple config, set up a network so we can get packages and re-run"
  echo; echo
  exit 1
else 
  echo "  We have $Interface, continuing"
fi

echo; echo;
echo Testing if we have network before continuing
echo; 
#
# Is there a default route?
#   This is the fast-fail
#
INET=`route | grep default`
echo "INET: $INET"
if [[ "$INET" == "" ]]; then
  echo; echo;
  echo "  We have NO network"
  echo; echo;
  exit 1
else
  echo "  We are configured at $INET, continuing"
fi

#
#
# Package Management Now
#
#
echo; echo 
echo Installing missing packages
echo; echo 

#
# We want Bash in / just as it should be
#
/usr/bin/pineapple/opkg update
/usr/bin/pineapple/opkg install bash 
/usr/bin/pineapple/opkg install --dest sd rsync unzip 
/usr/bin/pineapple/opkg install --dest sd vim-full coreutils-stty bully reaver cshark
/usr/bin/pineapple/opkg install --dest sd git git-http python-pip hcxdumptool hcxtools 


#
# Fix libpcap
#
#/usr/bin/pineapple/opkg remove tcpdump
#/usr/bin/pineapple/opkg remove libpcap --force-depends
/usr/bin/pineapple/opkg remove --force-all tcpdump libpcap
/usr/bin/pineapple/opkg install --dest sd libpcap
/usr/bin/pineapple/opkg install --dest sd tcpdump

#
# Fix Git, Git-Merge, Git-pull
#   Apparantly without this a number of GIT commands do not work right, even if they're in $PATH
#
if [[ ! -f /sd/usr/lib/git-core/git ]]; then
  ln -s /sd/usr/bin/git /sd/usr/lib/git-core/git
fi


#
# Fix some binary links that don't work with $PATH
#
# Lets fix our link to /sbin/tshark
if [[ ! -h /sbin/tshark ]]; then
  echo re-linking $SDMnt/sbin/cshark to /sbin/cshark
  ln -s $SDMnt/sbin/cshark /sbin/tshark
fi

#
# Fix Module Dependencies
#
for Loop in  `find $SDMnt/modules -iname dependencies.sh`
do
  echo "Executing $Loop"
  chmod 755 $Loop
  $Loop
done

#
# Get PMKIDAttack
#
echo 
echo 
echo "Get Latest Portals."
echo "There may be errorrs about templates.  That's ignorable"
echo 
if [[ -d $SDMnt/modules/PMKIDAttack ]]; then
  echo - Updating Existing
  cd $SDMnt/modules/PMKIDAttack
  git pull
else
  cd $SDMnt/modules
  echo - Pulling Portals from github
  git clone https://github.com/MelroyB/PMKIDAttack.git
fi

#
# Get Latest Portals
#
echo 
echo 
echo Get Latest Portals
if [[ -d $SDMnt/evilportals ]]; then
  echo - Updating Existing
  cd $SDMnt/evilportals
  git pull
  rsync -av $SDMnt/evilportals/portals/ /sd/portals/
else
  cd $SDMnt
  echo - Pulling Portals from github
  git clone https://github.com/kbeflo/evilportals.git 
fi

#
# Relink old Modules so we don't have to re-install again
#
if [[ -f $SDMnt/ReLink.sh ]]; then
  echo 
  echo 
  echo "Fixing Module Links to /sd"
  echo 
  $SDMnt/ReLink.sh
else
  echo
  echo "You are missing $SDMnet/ReLink.sh  i can't fix pre-exisiting Modules installed on the SD card."
fi

#
# Install wifite2
#   https://forums.hak5.org/topic/42244-wifite2-wifi-pineapple-setup/
#
echo 
echo 
echo Installing wifite2
if [ -d $SDMnt/wifite2 ]; then
  cd $SDMnt/wifite2/
  git pull
  cd $SDMnt/wifite2/
  chmod +x Wifite.py
else
  cd $SDMnt
  git clone git://github.com/derv82/wifite2.git
  cd $SDMnt/wifite2/
  chmod +x Wifite.py
fi
echo Execute with:
echo cd $SDMnt/wifite2
echo ./Wifite-ng 
echo
echo


#
# Done!
#
exit 0
