#!/bin/bash

#
# Where is our SD card mounted?  This is a standard but there may be one-offs
#
SD="sd"
SDMnt="/sd"
cd $SDMnt

echo "Sourcing $SDMnt/root_home/.profile"
. $SDMnt/root_home/.profile

#
# Fix Git, Git-Merge, Git-pull
#   Apparantly without this a number of GIT commands do not work right, even if they're in $PATH
#
if [[ ! -f /sd/usr/lib/git-core/git ]]; then
  ln -s /sd/usr/bin/git /sd/usr/lib/git-core/git
fi

#
#
# Package Management Now
#
#
echo; echo 
echo Installing missing packages
echo; echo 

#
# We want Bash in / just as it should be
#
/usr/bin/pineapple/opkg update
/usr/bin/pineapple/opkg install bash 
/usr/bin/pineapple/opkg install --dest sd rsync unzip 
/usr/bin/pineapple/opkg install --dest sd vim-full coreutils-stty bully reaver cshark
/usr/bin/pineapple/opkg install --dest sd git git-http 

#
# Fix libpcap
#
#  This will generate errors on the opkg files, they're ignorable
/usr/bin/pineapple/opkg remove --force-depends tcpdump libpcap
/usr/bin/pineapple/opkg install --dest sd libpcap tcpdump cshark dsniff 

#
# Fix Python Bits
#
PythonPackages="python-codecs python-compiler python-ctypes python-db python-decimal python-distutils python-email python-gdbm python-light python-logging python-multiprocessing python-ncurses python-openssl python-pip python-pydoc python-setuptools python-sqlite3 python-unittest python-xml"
/usr/bin/pineapple/opkg remove --force-depends $PythonPackages
/usr/bin/pineapple/opkg install --dest sd $PythonPackages

#
# Fix some binary links that don't work with $PATH
#
# Lets fix our link to /sbin/tshark
if [[ ! -h /sbin/tshark ]]; then
  echo re-linking $SDMnt/sbin/cshark to /sbin/cshark
  ln -s $SDMnt/sbin/cshark /sbin/tshark
fi

