#!/bin/bash

#
# Where is our SD card mounted?  This is a standard but there may be one-offs
#
SD="sd"
SDMnt="/sd"
cd $SDMnt

#
# Lets actually load $PATH, etc
#
echo "Sourcing $SDMnt/root_home/.profile"
. $SDMnt/root_home/.profile


#
# Fix Module Dependencies
#
for Loop in  `find $SDMnt/modules -iname dependencies.sh`
do
  echo "Executing $Loop"
  chmod 755 $Loop
  $Loop
done

#
# Get PMKIDAttack
#
echo 
echo 
echo "Get Latest Portals."
echo "There may be errorrs about templates.  That's ignorable"
echo 
if [[ -d $SDMnt/modules/PMKIDAttack ]]; then
  echo - Updating Existing
  cd $SDMnt/modules/PMKIDAttack
  git pull
else
  cd $SDMnt/modules
  echo - Pulling Portals from github
  git clone https://github.com/MelroyB/PMKIDAttack.git
fi

#
# Get Latest Portals
#
echo 
echo 
echo Get Latest Portals
if [[ -d $SDMnt/evilportals ]]; then
  echo - Updating Existing
  cd $SDMnt/evilportals
  git pull
  rsync -av $SDMnt/evilportals/portals/ /sd/portals/
else
  cd $SDMnt
  echo - Pulling Portals from github
  git clone https://github.com/kbeflo/evilportals.git 
fi

#
# Relink old Modules so we don't have to re-install again
#
if [[ -f $SDMnt/ReLink.sh ]]; then
  echo 
  echo 
  echo "Fixing Module Links to /sd"
  echo 
  $SDMnt/ReLink.sh
else
  echo
  echo "You are missing $SDMnet/ReLink.sh  i can't fix pre-exisiting Modules installed on the SD card."
fi

#
# Install wifite2
#   https://forums.hak5.org/topic/42244-wifite2-wifi-pineapple-setup/
#
echo 
echo 
echo Installing wifite2
if [ -d $SDMnt/wifite2 ]; then
  cd $SDMnt/wifite2/
  git pull
  cd $SDMnt/wifite2/
  chmod +x Wifite.py
else
  cd $SDMnt
  git clone git://github.com/derv82/wifite2.git
  cd $SDMnt/wifite2/
  chmod +x Wifite.py
fi
echo Execute with:
echo cd $SDMnt/wifite2
echo ./Wifite-ng 
echo
echo
