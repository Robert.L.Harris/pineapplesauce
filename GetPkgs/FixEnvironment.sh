#!/bin/bash

#
# Where is our SD card mounted?  This is a standard but there may be one-offs
#
SD="sd"
SDMnt="/sd"
cd $SDMnt

#
# Lets actually load $PATH, etc
#
if [[ -f $SDMnt/root_home/.profile ]]; then
  echo "Sourcing $SDMnt/root_home/.profile"
  . $SDMnt/root_home/.profile
else
  echo 
  echo "For Pathing, you really need top do the following:"
  echo "mkdir $SDMnt/root_home"
  echo "cp /root/.profile $SDMnt/root_home/.profile"
  echo "If you don't have a .profile, there is an example profile_sample in this repo"
  echo 
  exit 255
fi

#
# Is our sd card mounted
#
/bin/mount | /bin/grep $SDMnt >/dev/null 2>&1
MountCheck=$?
if [ "$MountCheck" -ne "0" ]; then
  echo; echo
  echo SD card is not mounted.
  echo; echo
  exit 1
fi

#
# Fix Paths
#
export LD_LIBRARY_PATH="/lib:/sd/lib:/usr/lib:/sd/usr/lib//usr/local/lib:$LD_LIBRARY_PATH"
export PATH="$PATH:/sbin:/usr/sbin:/sd/usr/sbin:/usr/local/bin:/root/bin:/bin:/sd/bin/:/sd/usr/bin/:/usr/bin/pineapple/opkg"

#
# Is our root shell still horked?  Lets fix it
#
/bin/grep bin/ash /etc/passwd >/dev/null 2>&1
PWCheck=$?
if [ "$PWCheck" -ne "1" ]; then
  echo "Fixing Password entry for root"
  /bin/sed -e 's,bin/ash,bin/bash,' < /etc/passwd > /tmp/passwd
  /bin/mv /tmp/passwd /etc/passwd
fi

#
# Fix /root, due to space, lets make it a link to /sd/root_home
#
if [[ -d $SDMnt/root_home ]]; then
  echo "You have a $SDMnt/root_home, linking to /root"
  rm -rf /root
  ln -s /sd/root_home  /root
else
  echo 
  echo 
  echo "You do not have a $SDMnt/root_home, NOT linking to /root"
  echo "** I recommend rsyncing /root to /sd/root_home so you can keep persistent root files in /sd/root"
  echo 
  echo 
fi

#
# Lets fix our link to /root/sd
#
if [[ ! -h /root/sd ]]; then
  echo re-linking $SDMnt to /root/sd
  ln -s $SDMnt /root/sd
fi
